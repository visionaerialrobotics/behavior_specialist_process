# Brief
This process is used as a database that contains all behavior information.
The process interprets a file written in the YAML language and save that configuration
in memory to be used later. This file is called `behavior_catalog.yaml`.

# Services
- **default_values** ([droneMsgsROS/ConsultDefaultBehaviorValues](https://bitbucket.org/joselusl/dronemsgsros/src/a6a289b1101d8a2de9799afe9c96cb540f524112/srv/ConsultDefaultBehaviorValues.srv))  
Returns the values assigned to the behavior passed.

- **check_group_consistency** ([droneMsgsROS/CheckBehaviorGroupConsistency](https://bitbucket.org/joselusl/dronemsgsros/src/a6a289b1101d8a2de9799afe9c96cb540f524112/srv/CheckBehaviorGroupConsistency.srv))  
Verifies if the behavior trying to activate is consistent with the behaviors already active.

- **check_reactive_activation** ([aerostack_msgs/CheckReactiveActivation](https://bitbucket.org/joselusl/dronemsgsros/src/a6a289b1101d8a2de9799afe9c96cb540f524112/srv/CheckReactiveActivation.srv))  
Returns the behaviors that can be active in a reactive way.

- **check_capabilities_consistency** ([droneMsgsROS/CheckCapabilitiesConsistency](https://bitbucket.org/joselusl/dronemsgsros/src/a6a289b1101d8a2de9799afe9c96cb540f524112/srv/CheckCapabilitiesConsistency.srv))  
Checks if the capability trying to activate is compatible with other capabilities already active.

- **behavior_resources** ([droneMsgsROS/QueryResources](https://bitbucket.org/joselusl/dronemsgsros/src/a6a289b1101d8a2de9799afe9c96cb540f524112/srv/QueryResources.srv))  
Returns the capability and process associated to a behavior.

- **check_behavior_format** ([aerostack_msgs/CheckBehaviorFormat](https://bitbucket.org/joselusl/dronemsgsros/src/8bde8999c8c84fb038c4a9e1bcfb5ddc97eadf96/srv/CheckBehaviorFormat.srv?at=master&fileviewer=file-view-default))
Return if the arguments associated to a behavior are correctly written.

# Parameters
- **config_file(string)**  
Contains the path of the catalog file that the process will use to get data of the behaviors.

---
# Contributors
**Maintainer:** Alberto Camporredondo (alberto.camporredondo@gmail.com)  
**Author:** Alberto Camporredondo
