/*!*******************************************************************************************
 *  \file       behavior_specialist_process.cpp
 *  \brief      BehaviorSpecialist implementation file.
 *  \details    This file implements the BehaviorSpecialist class.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#include "../include/behavior_specialist_process.h"

BehaviorSpecialist::BehaviorSpecialist()
{

}

BehaviorSpecialist::~BehaviorSpecialist()
{

}


void BehaviorSpecialist::ownSetUp()
{
  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory,
                  "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");
  private_nh.param<std::string>("executive_layer_configuration",executive_layer_configuration,
                                 "/configs/general/behavior_catalog.yaml");

  private_nh.param<std::string>("default_values_srv", default_values_str, "default_values");
  private_nh.param<std::string>("check_group_consistency_srv", check_group_consistency_str, "check_group_consistency");
  private_nh.param<std::string>("check_capabilities_consistency_srv", check_capabilities_consistency_str, "check_capability_consistency");
  private_nh.param<std::string>("behavior_resources_srv", behavior_resources_str, "behavior_resources");
  private_nh.param<std::string>("check_behavior_format_srv", check_behavior_format_str, "check_behavior_format");
  private_nh.param<std::string>("check_reactive_activation_srv", check_reactive_activation_str, "check_reactive_activation");
  private_nh.param<std::string>("consult_belief_cli", consult_belief_str, "consult_belief");
  private_nh.param<std::string>("record_reactive_behaviors_cli", record_reactive_behaviors_str, "record_reactive_behaviors");
  private_nh.param<std::string>("consult_available_behaviors_srv", consult_available_behaviors_str, "consult_available_behaviors");

  /*Initiate Behavior Catalog*/
  std::string executive_layer_configuration_path = my_stack_directory + executive_layer_configuration;
  behavior_catalog.loadConfiguration(executive_layer_configuration_path);

  ros::V_string available_nodes;
  ros::master::getNodes(available_nodes);
  behavior_catalog.checkProcesses(available_nodes, drone_id_namespace);
  behavior_catalog.checkBehaviors(available_nodes, drone_id_namespace);
}

void BehaviorSpecialist::ownStart()
{
  check_group_consistency_srv = node_handle.advertiseService(check_group_consistency_str, &BehaviorSpecialist::checkGroupConsistencyCallback, this);
  check_capabilities_consistency_srv = node_handle
    .advertiseService(check_capabilities_consistency_str, &BehaviorSpecialist::checkCapabilitiesConsistencyCallback, this);
  default_values_srv = node_handle.advertiseService(default_values_str, &BehaviorSpecialist::defaultValuesCallback, this);
  behavior_resources_srv = node_handle.advertiseService(behavior_resources_str, &BehaviorSpecialist::behaviorResourcesCallback, this);
  check_behavior_format_srv = node_handle.advertiseService(check_behavior_format_str, &BehaviorSpecialist::checkBehaviorFormatCallback, this);
  check_reactive_activation_srv = node_handle.advertiseService(check_reactive_activation_str, &BehaviorSpecialist::checkReactiveActivationCallback, this);
  consult_belief_cli = node_handle.serviceClient<droneMsgsROS::ConsultBelief>(consult_belief_str);
  record_reactive_behaviors_srv = node_handle.advertiseService(record_reactive_behaviors_str, &BehaviorSpecialist::recordReactiveBehaviorsCallback, this);
  consult_available_behaviors_srv = node_handle.advertiseService(consult_available_behaviors_str, &BehaviorSpecialist::consultAvailableBehaviorsCallback, this);
}

void BehaviorSpecialist::ownStop()
{
  check_group_consistency_srv.shutdown();
  check_capabilities_consistency_srv.shutdown();
  default_values_srv.shutdown();
  behavior_resources_srv.shutdown();
  check_behavior_format_srv.shutdown();
  consult_belief_cli.shutdown();

  behavior_catalog.eraseConfiguration();
}

void BehaviorSpecialist::ownRun()
{

}


/*Callback*/
bool BehaviorSpecialist::checkGroupConsistencyCallback(droneMsgsROS::CheckBehaviorGroupConsistency::Request &request,
                                                       droneMsgsROS::CheckBehaviorGroupConsistency::Response &response)
{
  std::cout << "Checking inconsistencies for behavior: {" << request.behavior_name << "}" << std::endl;

  bool found; std::string error_message; BehaviorDescriptor behavior;
  std::tie(found, error_message, behavior) = behavior_catalog.getBehaviorDescriptor(request.behavior_name);

  if(!found)
  {
    response.consistent = false;
    response.error_message = error_message;
    return true;
  }

  std::vector<std::string> incompatibilities;
  bool consistent = true;
  for(auto active_behavior : request.active_behaviors){
    for(auto incompatibility : behavior.getIncompatibilities()){
      if(active_behavior.name == incompatibility){
        std::cout << "Found inconsistency: "<< active_behavior.name << std::endl;
        incompatibilities.push_back(active_behavior.name);
        consistent = false;
      }
    }
  }
  response.consistent = consistent;
  response.incompatibilities = incompatibilities;
  return true;
}


bool BehaviorSpecialist::checkCapabilitiesConsistencyCallback(droneMsgsROS::CheckCapabilitiesConsistency::Request &request,
                                                              droneMsgsROS::CheckCapabilitiesConsistency::Response &response)
{
  std::vector<std::string> inconsistent_capabilities;
  std::string error_message = "The following capabilities are inconsistent: \n";

  for(auto capability_name : request.capabilities_to_activate){
    std::cout << "Checking inconsistencies for capability: {" << capability_name << "} " << std::endl;

    bool found; std::string error_message_capability; CapabilityDescriptor capability_to_activate;
    std::tie(found, error_message_capability, capability_to_activate) =
             behavior_catalog.getCapabilityDescriptor(capability_name);

    if(!found){
      response.consistent = false;
      response.error_message = error_message_capability;
      return true;
    }

    for(auto active_capability : request.active_capabilities){
      if(capability_name == active_capability){
        std::cout << "Found inconsistency: "<< active_capability << " " << std::endl;
          inconsistent_capabilities.push_back(active_capability);
          error_message += "Capability ["+active_capability+"]";
      }
      for(auto incompatibility : capability_to_activate.getIncompatibilities()){
        if(active_capability == incompatibility){
          std::cout << "Found inconsistency: "<< incompatibility << " " << std::endl;
          inconsistent_capabilities.push_back(incompatibility);
          error_message += "Capability ["+incompatibility+"]";
        }
      }
    }
  }
  if(inconsistent_capabilities.size() > 0){
    response.consistent = false;
    response.error_message = error_message;
  }
  else{
    response.consistent = true;
    response.error_message = "";
  }
  response.inconsistent_capabilities = inconsistent_capabilities;
  return true;
}


bool BehaviorSpecialist::defaultValuesCallback(droneMsgsROS::ConsultDefaultBehaviorValues::Request &request,
                                               droneMsgsROS::ConsultDefaultBehaviorValues::Response &response)
{
  std::cout << "Getting default values for behavior: {" << request.name << "} " << std::endl;

  bool found; std::string error_message; BehaviorDescriptor behavior;
  std::tie(found, error_message, behavior) = behavior_catalog.getBehaviorDescriptor(request.name);

  if(!found){
    response.ack = false;
    response.error_message = error_message;
    return true;
  }

  //response.recurrent = behavior.isRecurrent();
  response.timeout = behavior.getTimeout();
  response.ack = true;
  response.error_message = "";
  return true;
}

bool BehaviorSpecialist::behaviorResourcesCallback(droneMsgsROS::QueryResources::Request &request,
                                                   droneMsgsROS::QueryResources::Response &response)
{
  std::cout << "Getting capability data for behavior: {"<< request.behavior_name <<"} " << std::endl;

  bool found; std::string error_message; std::vector<CapabilityDescriptor> capabilities;
  std::tie(found, error_message, capabilities) = behavior_catalog.getCapabilities(request.behavior_name);

  if(!found){
    response.found = false;
    response.error_message = error_message;
    return true;
  }

  for(auto capability : capabilities){
    response.capabilities.push_back(capability.serialize());
  }
  response.found = true;
  response.error_message = "";
  return true;
}

bool BehaviorSpecialist::recordReactiveBehaviorsCallback(aerostack_msgs::RecordReactiveBehaviorsRequest& request,
                                                         aerostack_msgs::RecordReactiveBehaviorsResponse& response)
{
  /*Sending reactive behaviors*/
  std::cout << "Sending reactive behaviors " << std::endl;
  auto high_priority_reactive_behaviors = behavior_catalog.getHighPriorityReactiveBehaviors();
  auto low_priority_reactive_behaviors = behavior_catalog.getLowPriorityReactiveBehaviors();

  std::for_each(high_priority_reactive_behaviors.begin(), high_priority_reactive_behaviors.end(),
                [&](ReactiveBehaviorDescriptor behavior)
                {
		  aerostack_msgs::BehaviorCommand behavior_command;
		  behavior_command.name = behavior.getName();
		  behavior_command.arguments = behavior.getArguments();
                  response.high_priority_behaviors.push_back(behavior_command);
                });
  std::for_each(low_priority_reactive_behaviors.begin(), low_priority_reactive_behaviors.end(),
                [&](ReactiveBehaviorDescriptor behavior)
                {
		  aerostack_msgs::BehaviorCommand behavior_command;
		  behavior_command.name = behavior.getName();
		  behavior_command.arguments = behavior.getArguments();
                  response.low_priority_behaviors.push_back(behavior_command);
                });

  response.ack = true;
  return true;
}



bool BehaviorSpecialist::checkReactiveActivationCallback(aerostack_msgs::CheckReactiveActivation::Request& request,
                                                         aerostack_msgs::CheckReactiveActivation::Response& response)
{
  std::vector<aerostack_msgs::BehaviorCommand> compatible_behaviors;
  for (aerostack_msgs::BehaviorCommand behavior : request.reactive_behaviors)
  {
    std::cout << "Checking reactive activation for behavior [" << behavior << "] " << std::endl;

    bool retrieved; std::string error_message;
    ReactiveBehaviorDescriptor behavior_descriptor;
    std::tie(retrieved, error_message, behavior_descriptor) = behavior_catalog.getReactiveBehaviorDescriptor(behavior.name);

    if(!retrieved)
    {
      std::cout << "[ERROR]: " << error_message << std::endl;
      response.ack = false;
      response.error_message = error_message;
      return true;
    }

    std::cout << "Behavior: [" << behavior_descriptor.getName() << "] added as posible reactive activation " << std::endl;
    compatible_behaviors.push_back(behavior);

    if(behavior_descriptor.hasLowerPriority())
    {
      std::cout << "Behavior ["<< behavior.name <<"] has lower priority " << std::endl;

      bool retrieved; std::string error_message;
      BehaviorDescriptor behavior_descriptor;
      std::tie(retrieved, error_message, behavior_descriptor) = behavior_catalog.getBehaviorDescriptor(behavior.name);

      if(!retrieved)
      {
        std::cout << "[ERROR]: " << error_message << std::endl;
        response.ack = false;
        response.error_message = error_message;
        return true;
      }

      for(std::string incompatibility : behavior_descriptor.getIncompatibilities()){
	for(aerostack_msgs::BehaviorCommand requested_behavior : request.active_behaviors){
	  if(requested_behavior.name == incompatibility){
	    std::cout << "Behavior ["<<behavior.name<<"] is not compatible " << std::endl;
	    int offset = 0;
	    for(auto behavior_to_erase : compatible_behaviors){
	      if(behavior.name == behavior_to_erase.name){
		compatible_behaviors.erase(compatible_behaviors.begin()+offset);
		break;
	      }
	      offset++;
	    }
	    break;

	  }
	}
      }
    }
    else
      std::cout << "behavior ["<<behavior.name<<"] has higher priority " << std::endl;
  }

  std::vector<std::string> behaviors_to_activate, high_priority_behaviors, low_priority_behaviors;
  for(aerostack_msgs::BehaviorCommand behavior : compatible_behaviors)
  {
    bool retrieved; std::string error_message;
    ReactiveBehaviorDescriptor behavior_descriptor;
    std::tie(retrieved, error_message, behavior_descriptor) = behavior_catalog.getReactiveBehaviorDescriptor(behavior.name);

    if(!retrieved)
    {
      std::cout << "[ERROR]: " << error_message << std::endl;
      response.ack = false;
      response.error_message = error_message;
      return true;
    }

    droneMsgsROS::ConsultBelief consult_belief_msg;
    if(!behavior_descriptor.getCondition().empty())
    {
      consult_belief_msg.request.query = behavior_descriptor.getCondition();
      std::cout << "Consult query for behavior ["<<behavior_descriptor.getName()<<"]: "
                << consult_belief_msg.request.query << std::endl;

      consult_belief_cli.call(consult_belief_msg);
    }
    else
      consult_belief_msg.response.success = true;


    if(consult_belief_msg.response.success)
    {
      std::cout << "Behavior ["<<behavior.name<<"] will be active by coordinator " << std::endl;
      if(behavior_descriptor.hasHigherPriority())
        response.high_priority_behaviors.push_back(behavior);
      else
        response.low_priority_behaviors.push_back(behavior);
    }
    else{
      std::cout << "behavior ["<<behavior.name<<"] has an invalid condition " << std::endl;
    }
  }

  response.ack = true;
  return true;
}


bool BehaviorSpecialist::checkBehaviorFormatCallback(aerostack_msgs::CheckBehaviorFormat::Request& request,
						     aerostack_msgs::CheckBehaviorFormat::Response& response)
{
  bool found; std::string error_message;
  BehaviorDescriptor behavior;
  std::tie(found, error_message, behavior) =
  behavior_catalog.getBehaviorDescriptor(request.behavior.name);

  if(!found){
    response.ack = false;
    response.error_message = error_message;
    return true;
  }

  std::string argument = request.behavior.arguments;


  YAML::Node node;
  try{
    node = YAML::Load(argument);
  }
  catch(YAML::Exception exception){
    std::cout << "Argument does not follow YAML standards " << std::endl;
    response.ack = false;
    response.error_message = "Arguments for behavior \""+request.behavior.name+"\" does not follow YAML standards ";
    return true;
  }

  for (auto tag : node) {
    auto argument_name = tag.first.as<std::string>();
    //auto argument_values = node[argument_name]

    ArgumentDescriptor argument_descriptor;
    bool found = false;
    for(ArgumentDescriptor argument : behavior.getArguments()){
      if(argument.check_name(argument_name)){
        argument_descriptor.setName(argument.getName());
        argument_descriptor.setAllowedValues(argument.getAllowedValues());
        argument_descriptor.setDimensions(argument.getDimensions());
        found = true;
      }
    }

    if(!found){
      std::string error_message = "Incorrect argument \"" + argument_name + "\" for behavior \"" + request.behavior.name + "\"";
      response.ack = false;
      response.error_message = error_message;
      return true;
    }

    bool correct_format; std::string error_message;
    if(argument_name == "direction"){
      std::tie(correct_format, error_message) = argument_descriptor.check_string_format(node[argument_name].as<std::string>());
    }
    else{
	std::vector<std::string> params = node[argument_name].as<std::vector<std::string>>();
	bool has_variable = false;
	for(auto t : params){
	  std::cout << t << std::endl;
	  if(t.find("+") != std::string::npos){
	    has_variable = true;
	  }
	}

	if(has_variable){
	  std::tie(correct_format, error_message) = argument_descriptor.check_variable_format(params);
	}
	else{
	  try{
	    std::vector<double> coordinates = node[argument_name].as<std::vector<double>>();
	    std::tie(correct_format, error_message) = argument_descriptor.check_int_format(coordinates);
	  }catch(YAML::Exception e){
	    int number = node[argument_name].as<double>();
	    std::tie(correct_format, error_message) = argument_descriptor.check_int_format(number);
	  }
	}
    }

    if(!correct_format){
      response.ack = false;
      response.error_message = error_message + "for behavior \""+request.behavior.name+"\"";
      return true;
    }

  }
  response.ack = true;
  response.error_message = "";
  return true;
}


bool BehaviorSpecialist::consultAvailableBehaviorsCallback(droneMsgsROS::ConsultAvailableBehaviorsRequest& request,
                                                           droneMsgsROS::ConsultAvailableBehaviorsResponse& response)
{
  droneMsgsROS::ListOfBehaviors available_behaviors_msg;
  std::vector<BehaviorDescriptor> available_behaviors;

  available_behaviors = behavior_catalog.getBehaviors();

  std::for_each(available_behaviors.begin(), available_behaviors.end(),
                      [&](BehaviorDescriptor behavior){
                        available_behaviors_msg.behaviors.push_back(behavior.getName());
                      }
                );

  response.available_behaviors = available_behaviors_msg;
}
