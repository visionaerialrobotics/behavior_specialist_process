/*!*********************************************************************************
 *  \file       behavior_specialist_process.h
 *  \brief      BehaviorSpecialist definition file.
 *  \details    This file contains the BehaviorSpecialist declaration.
 *              To obtain more information about it's definition consult
 *              the behavior_specialist_process.cpp file.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#ifndef BEHAVIOR_SPECIALIST_PROCESS_H
#define BEHAVIOR_SPECIALIST_PROCESS_H

#include <algorithm>
#include <string>
#include <ros/ros.h>

#include <droneMsgsROS/QueryResources.h>
#include <droneMsgsROS/ConsultDefaultBehaviorValues.h>
#include <droneMsgsROS/CheckBehaviorGroupConsistency.h>
#include <droneMsgsROS/CheckCapabilitiesConsistency.h>
#include <aerostack_msgs/CheckBehaviorFormat.h>
#include <aerostack_msgs/CheckReactiveActivation.h>
#include <droneMsgsROS/ConsultBelief.h>
#include <aerostack_msgs/RecordReactiveBehaviors.h>
#include <droneMsgsROS/ConsultAvailableBehaviors.h>
#include <droneMsgsROS/ListOfBehaviors.h>

#include <drone_process.h>
#include "behavior_catalog.h"
#include "capability_descriptor.h"
#include "behavior_descriptor.h"
#include "argument_descriptor.h"
#include "reactive_behavior_descriptor.h"

class BehaviorSpecialist : public DroneProcess
{
private:
  ros::NodeHandle node_handle;

  std_srvs::Empty empty_msg;

  BehaviorCatalog behavior_catalog;

  std::string drone_id;
  std::string drone_id_namespace;
  std::string my_stack_directory;
  std::string executive_layer_configuration;

  std::string check_group_consistency_str;
  std::string check_capabilities_consistency_str;
  std::string default_values_str;
  std::string behavior_resources_str;
  std::string check_behavior_format_str;
  std::string check_reactive_activation_str;
  std::string consult_belief_str;
  std::string record_reactive_behaviors_str;
  std::string consult_available_behaviors_str;

  ros::ServiceServer default_values_srv;
  ros::ServiceServer check_group_consistency_srv;
  ros::ServiceServer check_capabilities_consistency_srv;
  ros::ServiceServer behavior_resources_srv;
  ros::ServiceServer check_behavior_format_srv;
  ros::ServiceServer check_reactive_activation_srv;
  ros::ServiceClient consult_belief_cli;
  ros::ServiceServer record_reactive_behaviors_srv;
  ros::ServiceServer consult_available_behaviors_srv;

public:
  BehaviorSpecialist();
  ~BehaviorSpecialist();

private://DroneProcess
  void ownSetUp();
  void ownStart();
  void ownRun();
  void ownStop();

public: //Callbacks
  bool behaviorResourcesCallback(droneMsgsROS::QueryResources::Request&, droneMsgsROS::QueryResources::Response&);
  bool defaultValuesCallback(droneMsgsROS::ConsultDefaultBehaviorValues::Request&, droneMsgsROS::ConsultDefaultBehaviorValues::Response&);
  bool checkGroupConsistencyCallback(droneMsgsROS::CheckBehaviorGroupConsistency::Request&, droneMsgsROS::CheckBehaviorGroupConsistency::Response&);
  bool checkCapabilitiesConsistencyCallback(droneMsgsROS::CheckCapabilitiesConsistency::Request&, droneMsgsROS::CheckCapabilitiesConsistency::Response&);
  bool checkReactiveActivationCallback(aerostack_msgs::CheckReactiveActivationRequest&, aerostack_msgs::CheckReactiveActivationResponse&);
  bool checkBehaviorFormatCallback(aerostack_msgs::CheckBehaviorFormat::Request&, aerostack_msgs::CheckBehaviorFormat::Response&);
  bool recordReactiveBehaviorsCallback(aerostack_msgs::RecordReactiveBehaviorsRequest&,
                                       aerostack_msgs::RecordReactiveBehaviorsResponse&);
  bool consultAvailableBehaviorsCallback(droneMsgsROS::ConsultAvailableBehaviorsRequest&,
                                          droneMsgsROS::ConsultAvailableBehaviorsResponse&);
};
#endif
